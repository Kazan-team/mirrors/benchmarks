#include "internals.h"

template void c11_atomics::benchmarks<std::uint16_t>(
    std::vector<Benchmark> &benches, const Config &config);
