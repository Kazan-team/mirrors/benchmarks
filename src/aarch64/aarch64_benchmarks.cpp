#include "aarch64_benchmarks.h"

#ifdef __aarch64__

std::vector<Benchmark> aarch64_benchmarks(const Config &config)
{
    std::vector<Benchmark> retval;
    // TODO: add aarch64 benchmarks
    (void)config;
    return retval;
}

#else

std::vector<Benchmark> aarch64_benchmarks(const Config &)
{
    return {};
}

#endif
