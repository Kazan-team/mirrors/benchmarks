#pragma once

#include <cassert>
#include <charconv>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <exception>
#include <functional>
#include <initializer_list>
#include <ios>
#include <iterator>
#include <memory>
#include <optional>
#include <ostream>
#include <string>
#include <string_view>
#include <system_error>
#include <type_traits>
#include <unordered_map>
#include <variant>
#include <vector>

enum class ResolveResult : bool
{
    Finished = false,
    MoreToResolve = true,
};

template <typename T> class LazyVec final
{
  private:
    struct Internals
    {
        std::vector<T> resolved_values;
        explicit Internals(std::vector<T> &&resolved_values)
            : resolved_values(std::move(resolved_values))
        {
        }
        explicit Internals(std::initializer_list<T> resolved_values = {})
            : resolved_values(resolved_values)
        {
        }
        virtual ~Internals() = default;
        virtual ResolveResult resolve_more()
        {
            return ResolveResult::Finished;
        }
    };
    template <
        typename Iterator, typename Sentinel,
        typename = std::enable_if_t<
            std::is_base_of_v<
                std::input_iterator_tag,
                typename std::iterator_traits<Iterator>::iterator_category> &&
            std::is_same_v<
                const typename std::iterator_traits<Iterator>::value_type,
                const T>>>
    struct LazyInternalsIter final : public Internals
    {
        struct State final
        {
            Iterator cur;
            Sentinel end;
            State(Iterator &&cur, Sentinel &&end) noexcept
                : cur(std::move(cur)), end(std::move(end))
            {
            }
        };
        std::optional<State> state;
        LazyInternalsIter(Iterator &&cur, Sentinel &&end) noexcept
            : state(std::in_place, std::move(cur), std::move(end))
        {
            if (state->cur == state->end)
            {
                state = std::nullopt;
            }
        }
        LazyInternalsIter(const LazyInternalsIter &) = delete;
        LazyInternalsIter &operator=(const LazyInternalsIter &) = delete;
        virtual ResolveResult resolve_more() override
        {
            if (!state)
            {
                return ResolveResult::Finished;
            }
            this->resolved_values.emplace_back(*state->cur++);
            if (state->cur == state->end)
            {
                state = std::nullopt;
                return ResolveResult::Finished;
            }
            return ResolveResult::MoreToResolve;
        }
    };
    template <
        typename Container,
        typename Iterator = decltype(std::begin(std::declval<Container>())),
        typename Sentinel = decltype(std::end(std::declval<Container>())),
        typename = decltype(LazyInternalsIter<Iterator, Sentinel>::cur)>
    struct LazyInternalsContainer final : public Internals
    {
        struct State final
        {
            Container container;
            Iterator cur; // must come after container
            Sentinel end; // must come after container
            explicit State(Container &&container) noexcept
                : container(std::move(container)),
                  cur(std::begin(this->container)),
                  end(std::begin(this->container))
            {
            }
            State(const State &) = delete;
            State &operator=(const State &) = delete;
        };
        std::optional<State> state;
        explicit LazyInternalsContainer(Container &&container) noexcept
            : state(std::in_place, std::move(container))
        {
            if (state->cur == state->end)
            {
                state = std::nullopt;
            }
        }
        virtual ResolveResult resolve_more() override
        {
            if (!state)
            {
                return ResolveResult::Finished;
            }
            this->resolved_values.emplace_back(*state->cur++);
            if (state->cur == state->end)
            {
                state = std::nullopt;
                return ResolveResult::Finished;
            }
            return ResolveResult::MoreToResolve;
        }
    };
    template <typename Fn,
              typename = std::enable_if_t<std::is_convertible_v<
                  decltype(std::declval<Fn &>()()), std::optional<T>>>>
    struct LazyInternalsFn final : public Internals
    {
        std::optional<Fn> fn;
        explicit LazyInternalsFn(Fn &&fn) noexcept
            : fn(std::in_place, std::move(fn))
        {
        }
        virtual ResolveResult resolve_more() override
        {
            if (!fn)
            {
                return ResolveResult::Finished;
            }
            if (std::optional<T> value = (*fn)())
            {
                this->resolved_values.emplace_back(std::move(*value));
                return ResolveResult::MoreToResolve;
            }
            fn = std::nullopt;
            return ResolveResult::Finished;
        }
    };
    std::shared_ptr<Internals> internals;

  public:
    LazyVec() noexcept : internals(std::make_shared<Internals>())
    {
    }
    LazyVec(std::vector<T> values) noexcept
        : internals(std::make_shared<Internals>(std::move(values)))
    {
    }
    LazyVec(std::initializer_list<T> values) noexcept
        : internals(std::make_shared<Internals>(values))
    {
    }
    template <typename Iterator, typename Sentinel,
              typename = decltype(LazyInternalsIter<Iterator, Sentinel>::cur)>
    LazyVec(Iterator begin, Sentinel end) noexcept
        : internals(std::make_shared<LazyInternalsIter<Iterator, Sentinel>>(
              std::move(begin), std::move(end)))
    {
    }
    template <typename Container, typename = decltype(LazyInternalsContainer<
                                                      Container>::container)>
    LazyVec(Container container) noexcept
        : internals(std::make_shared<LazyInternalsContainer<Container>>(
              std::move(container)))
    {
    }
    template <typename Fn, typename = void,
              typename = decltype(LazyInternalsFn<Fn>::fn)>
    LazyVec(Fn fn) noexcept
        : internals(std::make_shared<LazyInternalsFn<Fn>>(std::move(fn)))
    {
    }

  private:
    static bool resolve_at(const std::shared_ptr<Internals> &internals,
                           std::size_t index) noexcept
    {
        while (index >= internals->resolved_values.size())
        {
            switch (internals->resolve_more())
            {
            case ResolveResult::Finished:
                goto end;
            case ResolveResult::MoreToResolve:
                continue;
            }
        }
    end:
        return index < internals->resolved_values.size();
    }

  public:
    bool resolve_at(std::size_t index) noexcept
    {
        return resolve_at(internals, index);
    }
    const T &at(std::size_t index) noexcept
    {
        if (!resolve_at(index))
        {
            assert(!"index out of bounds");
            std::terminate();
        }
        return internals->resolved_values[index];
    }
    const T &operator[](std::size_t index) noexcept
    {
        return at(index);
    }
    std::size_t size_lower_bound() noexcept
    {
        return internals->resolved_values.size();
    }
    ResolveResult resolve_more()
    {
        return internals->resolve_more();
    }
    const std::vector<T> &resolve()
    {
        while (true)
        {
            switch (resolve_more())
            {
            case ResolveResult::Finished:
                return internals->resolved_values;
            case ResolveResult::MoreToResolve:
                continue;
            }
        }
    }
    class const_iterator final
    {
        friend class LazyVec;

      private:
        std::shared_ptr<Internals> internals;
        std::size_t index = 0;
        bool resolve() const noexcept
        {
            assert(internals);
            if (!internals)
            {
                std::terminate();
            }
            return resolve_at(internals, index);
        }
        explicit const_iterator(
            const std::shared_ptr<Internals> &internals) noexcept
            : internals(internals)
        {
        }

      public:
        using difference_type = std::ptrdiff_t;
        using value_type = T;
        using pointer = const T *;
        using reference = const T &;
        using iterator_category = std::forward_iterator_tag;

        const_iterator() noexcept = default;
        const_iterator &operator++()
        {
            assert(internals);
            ++index;
            return *this;
        }
        const_iterator operator++(int)
        {
            auto retval = *this;
            operator++();
            return retval;
        }
        pointer operator->() const noexcept
        {
            if (at_end())
            {
                assert(!"tried to dereference an end() iterator");
                std::terminate();
            }
            return std::addressof(internals->resolved_values[index]);
        }
        reference operator*() const noexcept
        {
            return *operator->();
        }
        bool at_end() const noexcept
        {
            return !resolve();
        }
        bool operator==(const const_iterator &rhs) const noexcept
        {
            if (rhs.internals)
            {
                if (internals)
                {
                    assert(internals == rhs.internals);
                    return index == rhs.index;
                }
                return rhs.at_end();
            }
            if (internals)
            {
                return at_end();
            }
            return true;
        }
        bool operator!=(const const_iterator &rhs) const noexcept
        {
            return !operator==(rhs);
        }
    };
    const_iterator begin() const noexcept
    {
        return const_iterator(internals);
    }
    const_iterator cbegin() const noexcept
    {
        return const_iterator(internals);
    }
    const_iterator end() const noexcept
    {
        return const_iterator();
    }
    const_iterator cend() const noexcept
    {
        return const_iterator();
    }
    template <typename R, typename Fn> LazyVec<R> filter_map(Fn &&fn) noexcept
    {
        struct FilterMapper final
        {
            const_iterator iter;
            Fn fn;
            std::optional<R> operator()() noexcept
            {
                while (iter != const_iterator())
                {
                    if (std::optional<R> retval = fn(*iter++))
                    {
                        return retval;
                    }
                }
                return std::nullopt;
            }
        };
        return LazyVec<R>(FilterMapper{.iter = begin(), .fn = std::move(fn)});
    }
    template <typename R, typename Fn> LazyVec<R> map(Fn fn) noexcept
    {
        return filter_map<R>(
            [fn](const T &value) -> std::optional<R> { return fn(value); });
    }
    template <typename Fn> LazyVec filter(Fn fn) noexcept
    {
        return filter_map<T>([fn](const T &value) -> std::optional<T> {
            if (fn(value))
                return value;
            return std::nullopt;
        });
    }
};

struct JsonValue;

struct JsonValue final
{
    using String = std::string;
    using Number = double;
    using Object = LazyVec<std::pair<String, JsonValue>>;
    using Array = LazyVec<JsonValue>;
    using Bool = bool;
    using Null = std::nullptr_t;
    std::variant<String, Number, Object, Array, Bool, Null> value;
    constexpr JsonValue() noexcept : value(nullptr)
    {
    }
    JsonValue(String value) noexcept : value(std::move(value))
    {
    }
    JsonValue(const char *value) noexcept : value(std::string(value))
    {
    }
    JsonValue(std::string_view value) noexcept : value(std::string(value))
    {
    }
    constexpr JsonValue(Number value) noexcept : value(value)
    {
    }
    JsonValue(Object value) noexcept : value(std::move(value))
    {
    }
    JsonValue(Array value) noexcept : value(std::move(value))
    {
    }
    constexpr JsonValue(Bool value) noexcept : value(value)
    {
    }
    constexpr JsonValue(Null) noexcept : value(nullptr)
    {
    }
    constexpr JsonValue(char) noexcept = delete;
    constexpr JsonValue(char16_t) noexcept = delete;
    constexpr JsonValue(char32_t) noexcept = delete;
#define JSON_VALUE_NUM(T)                                                     \
    constexpr JsonValue(T value) noexcept                                     \
        : JsonValue(static_cast<double>(value))                               \
    {                                                                         \
    }
    JSON_VALUE_NUM(float)
    JSON_VALUE_NUM(long double)
    JSON_VALUE_NUM(unsigned char)
    JSON_VALUE_NUM(signed char)
    JSON_VALUE_NUM(unsigned short)
    JSON_VALUE_NUM(short)
    JSON_VALUE_NUM(unsigned int)
    JSON_VALUE_NUM(int)
    JSON_VALUE_NUM(unsigned long)
    JSON_VALUE_NUM(long)
    JSON_VALUE_NUM(unsigned long long)
    JSON_VALUE_NUM(long long)
#undef JSON_VALUE_NUM
    template <typename T, typename = decltype(JsonValue(std::declval<T &&>()))>
    constexpr JsonValue(std::optional<T> value) noexcept : value(nullptr)
    {
        if (value)
        {
            *this = JsonValue(std::move(*value));
        }
    }
    template <typename T,
              typename = decltype(JsonValue(std::declval<const T &>()))>
    JsonValue(LazyVec<T> value) noexcept
        : value(value.template map<JsonValue>(
              [](const T &value) { return JsonValue(value); }))
    {
    }
    template <typename T,
              typename = decltype(JsonValue(std::declval<const T &>()))>
    JsonValue(std::vector<T> value) noexcept
        : JsonValue(LazyVec<T>(std::move(value)))
    {
    }
    /// decode a JsonString from WTF-8 to the encoding logically used in JSON:
    /// WTF-16 aka. potentially ill-formed UTF-16
    ///
    /// https://simonsapin.github.io/wtf-8/
    class JsonStringDecoder final
    {
      private:
        std::string_view remaining;
        bool last_was_lead_surrogate = false;
        std::optional<std::uint16_t> trail_surrogate;

      public:
        constexpr JsonStringDecoder() noexcept : JsonStringDecoder("")
        {
        }
        constexpr explicit JsonStringDecoder(
            std::string_view json_string) noexcept
            : remaining(json_string)
        {
        }

      private:
        [[noreturn]] void throw_wtf8_err()
        {
            *this = JsonStringDecoder();
            throw std::ios_base::failure("Invalid WTF-8");
        }

      public:
        constexpr std::optional<std::uint16_t> next()
        {
            if (trail_surrogate)
            {
                auto retval = *trail_surrogate;
                trail_surrogate = std::nullopt;
                return retval;
            }
            if (remaining.empty())
            {
                return std::nullopt;
            }
            std::uint32_t code_point = 0;
            struct min_max final
            {
                std::uint8_t min = 0x80;
                std::uint8_t max = 0xBF;
            };
            auto get = [&]() -> std::uint8_t {
                if (remaining.empty())
                {
                    throw_wtf8_err();
                }
                std::uint8_t retval = remaining[0];
                remaining.remove_prefix(1);
                return retval;
            };
            auto cont = [&](min_max min_max = {}) {
                std::uint8_t v = get();
                if (v < min_max.min || v > min_max.max)
                {
                    throw_wtf8_err();
                }
                code_point <<= 6;
                code_point |= v & 0x3F;
            };
            std::uint8_t initial = get();
            if (initial < 0x80)
            {
                code_point = initial;
            }
            else if (initial < 0xC2)
            {
                throw_wtf8_err();
            }
            else if (initial < 0xE0)
            {
                code_point = initial & 0x1F;
                cont();
            }
            else if (initial == 0xE0)
            {
                code_point = initial & 0xF;
                cont({.min = 0xA0});
                cont();
            }
            else if (initial < 0xF0)
            {
                code_point = initial & 0xF;
                cont();
                cont();
            }
            else if (initial == 0xF0)
            {
                code_point = initial & 0x7;
                cont({.min = 0x90});
                cont();
                cont();
            }
            else if (initial < 0xF4)
            {
                code_point = initial & 0x7;
                cont();
                cont();
                cont();
            }
            else if (initial == 0xF4)
            {
                code_point = initial & 0x7;
                cont({.max = 0x8F});
                cont();
                cont();
            }
            else
            {
                throw_wtf8_err();
            }
            if (last_was_lead_surrogate && code_point >= 0xDC00 &&
                code_point <= 0xDFFF)
            {
                // got lead surrogate followed by trail surrogate --
                // invalid in WTF-8
                throw_wtf8_err();
            }
            last_was_lead_surrogate =
                (code_point >= 0xD800 && code_point <= 0xDBFF);
            bool is_supplementary_code_point = code_point >= 0x10000;
            if (is_supplementary_code_point)
            {
                auto value = code_point - 0x10000;
                std::uint16_t retval = (value >> 10) + 0xD800;
                trail_surrogate = (value & 0x3FF) + 0xDC00;
                return retval;
            }
            else
            {
                return code_point;
            }
        }
    };

  private:
    template <typename WriteStringView> struct Visitor final
    {
        WriteStringView &write_fn;
        bool pretty;
        std::size_t indent = 0;
        void write(std::string_view str)
        {
            write_fn(str);
        }
        void write(char ch)
        {
            write_fn(std::string_view(&ch, 1));
        }
        void write_indent()
        {
            for (std::size_t i = 0; i < indent; i++)
            {
                write("    ");
            }
        }
        void operator()(const String &value)
        {
            write('\"');
            JsonStringDecoder decoder(value);
            while (auto value_opt = decoder.next())
            {
                std::uint16_t value = *value_opt;
                switch (value)
                {
                case '\"':
                case '\\':
                    write('\\');
                    write(static_cast<char>(value));
                    break;
                case '\b':
                    write("\\b");
                    break;
                case '\f':
                    write("\\f");
                    break;
                case '\n':
                    write("\\n");
                    break;
                case '\r':
                    write("\\r");
                    break;
                case '\t':
                    write("\\t");
                    break;
                default:
                    if (value >= 0x20 && value <= 0x7E)
                    {
                        write(static_cast<char>(value));
                    }
                    else
                    {
                        static constexpr char hex_digits[] =
                            "0123456789ABCDEF";
                        write("\\u");
                        for (int i = 0; i < 4; i++)
                        {
                            write(hex_digits[value >> 12]);
                            value <<= 4;
                        }
                    }
                    break;
                }
            }
            write('"');
        }
        void operator()(Number value)
        {
            if (std::isnan(value))
            {
                write("NaN");
                return;
            }
            if (std::signbit(value))
            {
                write('-');
                value = -value;
            }
            if (std::isinf(value))
            {
                write("Infinity");
                return;
            }
            if (value == 0)
            {
                write("0");
                return;
            }
            using Buf = std::array<char, 32>;
            auto try_format = [&](Buf &buf, bool e_format, int prec) -> bool {
                int result;
                if (e_format)
                {
                    result = std::snprintf(&buf[0], buf.size(), "%1.*e", prec,
                                           value);
                }
                else
                {
                    result = std::snprintf(&buf[0], buf.size(), "%1.*f", prec,
                                           value);
                }
                if (result <= 0)
                    return false;
                double parsed_value = std::strtod(&buf[0], nullptr);
                if (parsed_value != value)
                {
                    // not precise enough
                    return false;
                }
                return true;
            };
            Buf final_buf = {};
            std::optional<std::string_view> final;
            std::size_t end_prec = final_buf.size();
            for (std::size_t prec = 0;
                 prec < final_buf.size() && prec < end_prec; prec++)
            {
                Buf buf;
                if (try_format(buf, true, prec))
                {
                    std::string_view str(&buf[0]);
                    if (!final || str.size() < final->size())
                    {
                        final_buf = buf;
                        final = std::string_view(&final_buf[0], str.size());
                        end_prec = prec + 3;
                    }
                }
                if (try_format(buf, false, prec))
                {
                    std::string_view str(&buf[0]);
                    if (!final || str.size() < final->size())
                    {
                        final_buf = buf;
                        final = std::string_view(&final_buf[0], str.size());
                        end_prec = prec + 3;
                    }
                }
            }
            if (final_buf[0] == '.')
            {
                write('0');
            }
            write(*final);
        }
        void operator()(Bool value)
        {
            write(value ? "true" : "false");
        }
        void operator()(Null)
        {
            write("null");
        }
        template <typename T, typename Fn>
        void write_container_inner(const LazyVec<T> &value, Fn fn)
        {
            indent++;
            std::string_view sep{};
            bool any = false;
            for (auto &v : value)
            {
                any = true;
                write(sep);
                if (pretty)
                {
                    write('\n');
                    write_indent();
                }
                sep = ",";
                fn(v);
            }
            indent--;
            if (pretty && any)
            {
                write('\n');
                write_indent();
            }
        }
        void operator()(const Array &value)
        {
            write('[');
            write_container_inner(value, [&](const JsonValue &value) {
                std::visit(*this, value.value);
            });
            write(']');
        }
        void operator()(const Object &value)
        {
            write('{');
            write_container_inner(
                value, [&](const std::pair<String, JsonValue> &value) {
                    operator()(value.first);
                    write(':');
                    if (pretty)
                    {
                        write(' ');
                    }
                    std::visit(*this, value.second.value);
                });
            write('}');
        }
    };

  public:
    template <typename WriteStringView>
    void write(WriteStringView &&write_fn, bool pretty = false) const
    {
        std::visit(
            Visitor<WriteStringView>{
                .write_fn = write_fn,
                .pretty = pretty,
            },
            value);
    }
    struct PrettyJsonValue;
    PrettyJsonValue pretty(bool pretty = true) const noexcept;
    friend std::ostream &operator<<(std::ostream &os, const JsonValue &self)
    {
        self.write([&](std::string_view str) { os << str; });
        return os;
    }
};

struct JsonValue::PrettyJsonValue final
{
    JsonValue value;
    bool pretty;
    friend std::ostream &operator<<(std::ostream &os,
                                    const PrettyJsonValue &self)
    {
        self.value.write([&](std::string_view str) { os << str; },
                         self.pretty);
        return os;
    }
};

inline auto JsonValue::pretty(bool pretty) const noexcept -> PrettyJsonValue
{
    return {.value = *this, .pretty = pretty};
}