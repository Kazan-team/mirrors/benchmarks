#pragma once

#include "../harness.h"

std::vector<Benchmark> x86_64_benchmarks(const Config &config);
