#!/bin/bash
set -e
arches="$*"
if [[ "$arches" == "" ]]; then
    arches="x86_64 aarch64 ppc64le"
fi

# need to install g++ first so the local arch will get filtered out later
which g++ > /dev/null || (set -x; sudo apt-get install -y build-essential g++)

needed=()

for arch in $arches; do
    case "$arch" in
    x86_64)
        which x86_64-linux-gnu-g++ > /dev/null || needed+=(g++-x86-64-linux-gnu)
        ;;
    aarch64)
        which aarch64-linux-gnu-g++ > /dev/null || needed+=(g++-aarch64-linux-gnu)
        ;;
    ppc64le)
        which powerpc64le-linux-gnu-g++ > /dev/null || needed+=(g++-powerpc64le-linux-gnu)
        ;;
    *)
        echo "unknown arch: $arch" >&2
        exit 1
    esac
done
which clang++-11 > /dev/null || needed+=(clang-11)
which make > /dev/null || needed+=(make)
which cmake > /dev/null || needed+=(cmake)
which ccache > /dev/null || needed+=(ccache)

if ((${#needed[@]})); then
    (set -x; sudo apt-get install -y "${needed[@]}")
fi
