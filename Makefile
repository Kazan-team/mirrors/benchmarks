.PHONY: all clean configure

host_arch = $(shell uname -m)
ifeq ($(host_arch),x86_64)
	enabled_arches = ppc64le aarch64 x86_64
else ifeq ($(host_arch),aarch64)
	# debian 10 doesn't have cross-compilers for ppc64le from aarch64
	enabled_arches = aarch64 x86_64
else ifeq ($(host_arch),ppc64le)
	enabled_arches = ppc64le aarch64 x86_64
else
	$(error unsupported arch $(host_arch))
endif

all: $(foreach arch,$(enabled_arches),build-$(arch)/benchmarks)

common_cmake_flags = -S .
common_cmake_flags += -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE
common_cmake_flags += -DCMAKE_BUILD_TYPE=RelWithDebInfo

reset_make_env = "MAKEFLAGS=" "MFLAGS=" "MAKELEVEL=" "MAKE_TERMERR=" "MAKE_TERMOUT="

.installed-dependencies: install-deps.sh Makefile
	./install-deps.sh $(enabled_arches)
	touch .installed-dependencies

build-ppc64le/Makefile: toolchain-powerpc64le-linux-gnu.cmake CMakeLists.txt .installed-dependencies
	rm -fr build-ppc64le
	env $(reset_make_env) cmake $(common_cmake_flags) -B build-ppc64le -DCMAKE_TOOLCHAIN_FILE=toolchain-powerpc64le-linux-gnu.cmake
	[ "$(host_arch)" != "ppc64le" ] || cp build-ppc64le/compile_commands.json .

build-aarch64/Makefile: toolchain-aarch64-linux-gnu.cmake CMakeLists.txt .installed-dependencies
	rm -fr build-aarch64
	env $(reset_make_env) cmake $(common_cmake_flags) -B build-aarch64 -DCMAKE_TOOLCHAIN_FILE=toolchain-aarch64-linux-gnu.cmake
	[ "$(host_arch)" != "aarch64" ] || cp build-aarch64/compile_commands.json .

build-x86_64/Makefile: toolchain-x86_64-linux-gnu.cmake CMakeLists.txt .installed-dependencies
	rm -fr build-x86_64
	env $(reset_make_env) cmake $(common_cmake_flags) -B build-x86_64 -DCMAKE_TOOLCHAIN_FILE=toolchain-x86_64-linux-gnu.cmake
	[ "$(host_arch)" != "x86_64" ] || cp build-x86_64/compile_commands.json .

configure: $(foreach arch,$(enabled_arches),build-$(arch)/Makefile)

.PHONY: __force-run

build-ppc64le/benchmarks: build-ppc64le/Makefile __force-run
	$(MAKE) -C build-ppc64le benchmarks

build-aarch64/benchmarks: build-aarch64/Makefile __force-run
	$(MAKE) -C build-aarch64 benchmarks

build-x86_64/benchmarks: build-x86_64/Makefile __force-run
	$(MAKE) -C build-x86_64 benchmarks

clean:
	rm -fr build-ppc64le build-aarch64 build-x86_64 .installed-dependencies compile_commands.json

